function integrand_NFW_cumulative_PDF,x

  COMMON concentration,c

  prefactor = (c^3.)/(alog(1.+c) - (c/(1.+c)))
  numerator = x^2
  denominator = (c*x)*(1. + c*x)^2.
  integrand = prefactor*numerator/denominator

return,integrand
end
;**********************************************
pro create_NFW_lookup_table,conc,logradius_table,logNFW_cumulative_PDF_table

  logrmin = -4
  logrmax = -0.01
  Npts = 100
  logradius_table = findgen(Npts+1)*(logrmax-logrmin)/double(Npts) + logrmin
  radius_table = 10.^logradius_table

  COMMON concentration,c
  c = conc

  NFW_cumulative_PDF_table = QROMO('integrand_NFW_cumulative_PDF',0.0,radius_table,/double)
  logNFW_cumulative_PDF_table = alog10(NFW_cumulative_PDF_table)

return
end
;**********************************************
pro draw_NFW_radial_positions,logradius_table,logNFW_cumulative_PDF_table,Ngals,radial_positions

  y = RandomU(seed,Ngals,/double)
  logy = alog10(y)
  logradial_positions = INTERPOL(logradius_table,logNFW_cumulative_PDF_table,logy,/spline)
  radial_positions = 10.^logradial_positions

return
end

;**********************************************

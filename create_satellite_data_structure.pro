pro create_satellite_data_structure,host_halos,satellites
;*** Initialize the satellite data structure

  Nsats = TOTAL(host_halos.Nsat)
  strtemp = {ID:0LL,hostID:0LL,hostpos:fltarr(3),hostvel:fltarr(3),hostconc:0.,rvir:0.,Mvir:0.,hostgalyn:0,radialpos:0.,halopos:fltarr(3),angles:fltarr(2),pos:fltarr(3),vel:fltarr(3),iquench:0,iquenchcen:0}
  satellites = Replicate(strtemp,Nsats)

return
end

pro assign_host_properties_to_satellites,host_halos,satellites
;*** Simple loop over satellites
;*** Assigns the property of a central to its satellites
;*** according to tags created in create_satellite_data_structure

  Nsats = n_elements(satellites.ID)
  satellites.ID = findgen(Nsats)
;
  whcen = where(host_halos.Nsat gt 0,Ncen)
  centrals = host_halos[whcen]
;
  first_index=0LL
  FOR ii=0LL,Ncen-1 DO BEGIN
     cenii = centrals[ii]
     last_index = first_index + cenii.Nsat - 1
     satsii = satellites[first_index:last_index]
     satsii.hostID = cenii.ID
     satsii.hostpos = cenii.pos
     satsii.hostvel = cenii.vel
     satsii.hostconc = cenii.conc
     satsii.rvir = cenii.Rvir
     satsii.Mvir = cenii.Mvir
     satsii.hostgalyn = cenii.Ncen
     satellites[first_index:last_index] = satsii
     first_index = last_index + 1
  ENDFOR

return
end

pro generate_mock_satellite_profile,conc,radius,relative_number_counts
;--- Must use linearly spaced bins! (Why, again?)
 
  logrmin = -3
  logrmax = 0
  rmax = 10.^logrmax
  rmin = 10.^logrmin
  npts=100
  rbins = findgen(npts)*(rmax-rmin)/double(npts-1) + rmin
  shifted_rbins = SHIFT(rbins,-1)
  dr = shifted_rbins - rbins
  dr = dr[0:npts-2]
  radius = rbins[0:npts-2] + 0.5*dr
  
  relative_number_counts = radius*((1+conc)^2)/((1+conc*radius)^2)
  
return
end

;***************************************************
pro assign_radialpos_to_satellites,satellites,Fconc
;*** This is the slowest routine in the HOD code by an order of magnitude
;*** There must be a smarter way to generate a satellite population
;*** with an NFW radial profile
;*** I wrote this routine myself but I still don't fully understand 
;*** how it works
;*** Based on my genpop2 routine

  wh_sort = sort(satellites.hostID)
  satellites = satellites[wh_sort]
  wh_uniq = UNIQ(satellites.hostID)
  Nsystems = n_elements(wh_uniq)

;*** Start with first host system
  first_index = 0
  last_index = wh_uniq[0]
  satsii = satellites[first_index:last_index]
  generate_mock_satellite_profile,Fconc*satsii[0].hostconc,radiusray,profileray
  host_centric_distancesii = genpop2(radiusray,profileray,last_index-first_index+1)
  satsii.radialpos = host_centric_distancesii
  satellites[first_index:last_index] = satsii

;*** Loop over all other host systems
  FOR ii=1,Nsystems-1 DO BEGIN
     first_index = last_index+1
     last_index = wh_uniq[ii]
     satsii = satellites[first_index:last_index]
     generate_mock_satellite_profile,Fconc*satsii[0].hostconc,radiusray,profileray
     host_centric_distancesii = genpop2(radiusray,profileray,last_index-first_index+1)
     satsii.radialpos = host_centric_distancesii
     satellites[first_index:last_index] = satsii
  ENDFOR


return
end

;***************************************************
pro new_fast_assign_radialpos_to_satellites,satellites,Fconc
;*** Uses PDF transformation method described in 
;--- Section 3.7 of AstroML text

  minconc = min(satellites.hostconc)-0.01
  maxconc = max(satellites.hostconc)+0.01
  dconc = 0.25
  Npts = ROUND((maxconc-minconc)/dconc)
  concbins = minconc + findgen(Npts+1)*(maxconc-minconc)/double(npts)

  FOR ii=0,Npts-1 DO BEGIN
     whii = where(satellites.hostconc ge concbins[ii] and satellites.hostconc lt concbins[ii+1],ctii)
     IF (ctii gt 0) THEN BEGIN
        satsii = satellites[whii]
        median_conc = median(satsii.hostconc)*Fconc
        create_NFW_lookup_table,median_conc,radius_table,NFW_cumulative_PDF_table
        draw_NFW_radial_positions,radius_table,NFW_cumulative_PDF_table,ctii,radial_positions
        satsii.radialpos = radial_positions
        satellites[whii] = satsii
     ENDIF

  ENDFOR


return
end

;***************************************************
pro assign_spatial_positions_to_satellites,satellites,Lbox

  Nsats = n_elements(satellites.ID)

;*** Choose random points on the unit 2-sphere for each satellite
  pi = 3.1415927
;--- azimuthal angles are drawn from a uniform distribution spanning [0,2pi]
  random_azimuthal_angles = RandomU(seed1,Nsats,/double)*2.0*pi
;--- cosine of polar angles are drawn from a uniform distribution spanning [-1,1]
  random_cosine_polar_angles = RandomU(seed2,Nsats,/double)*2 - 1
  random_polar_angles = acos(random_cosine_polar_angles)
;
  satellites.angles[0] = random_azimuthal_angles
  satellites.angles[1] = random_polar_angles
;*** Convert unit 2-sphere coordinates to host halo-centric positions in units of Rvir
  satellites.halopos[0] = satellites.radialpos*cos(satellites.angles[0])*sin(satellites.angles[1])
  satellites.halopos[1] = satellites.radialpos*sin(satellites.angles[0])*sin(satellites.angles[1])
  satellites.halopos[2] = satellites.radialpos*cos(satellites.angles[1])
;*** Convert halopos into units of Mpc/h
  satellites.halopos[0] = satellites.halopos[0]*satellites.rvir/1000.0
  satellites.halopos[1] = satellites.halopos[1]*satellites.rvir/1000.0
  satellites.halopos[2] = satellites.halopos[2]*satellites.rvir/1000.0
;*** Convert host halo-centric positions to actual positions
  satellites.pos[0] = satellites.halopos[0] + satellites.hostpos[0]
  satellites.pos[1] = satellites.halopos[1] + satellites.hostpos[1]
  satellites.pos[2] = satellites.halopos[2] + satellites.hostpos[2]
;*** Impose periodic boundary conditions
  wh_negx = where(satellites.pos[0] lt 0,ct_negx)
  IF (ct_negx gt 0) THEN satellites[wh_negx].pos[0] = Lbox - satellites[wh_negx].pos[0]
  wh_negy = where(satellites.pos[1] lt 0,ct_negy)
  IF (ct_negy gt 0) THEN satellites[wh_negy].pos[1] = Lbox - satellites[wh_negy].pos[1]
  wh_negz = where(satellites.pos[2] lt 0,ct_negz)
  IF (ct_negz gt 0) THEN satellites[wh_negz].pos[2] = Lbox - satellites[wh_negz].pos[2]
;
  wh_toobigx = where(satellites.pos[0] gt Lbox,ct_toobigx)
  IF (ct_toobigx gt 0) THEN satellites[wh_toobigx].pos[0] = satellites[wh_toobigx].pos[0] - Lbox
  wh_toobigy = where(satellites.pos[1] gt Lbox,ct_toobigy)
  IF (ct_toobigy gt 0) THEN satellites[wh_toobigy].pos[1] = satellites[wh_toobigy].pos[1] - Lbox
  wh_toobigz = where(satellites.pos[2] gt Lbox,ct_toobigz)
  IF (ct_toobigz gt 0) THEN satellites[wh_toobigz].pos[2] = satellites[wh_toobigz].pos[2] - Lbox
;
return
end

;----------------

pro assign_velocities_to_satellites,satellites
;*** Just assign velocity of central galaxy
;*** Can be improved upon if ever interested in z-space distortions
;*** No improvement is necessary for projected clustering with pi_max = 40Mpc

  satellites.vel = satellites.hostvel

return
end

;----------------

pro test_satellite_hostcentric_positions,satellites,Lbox
 
;*** Compute host-centric positions
  xdiff = satellites.pos[0]-satellites.hostpos[0]
  ydiff = satellites.pos[1]-satellites.hostpos[1]
  zdiff = satellites.pos[2]-satellites.hostpos[2]

;*** Correct for periodic boundary conditions, which is annoyingly complicated

;-- store absolute values of differences
  absxdiff = ABS(xdiff)    
  absydiff = ABS(ydiff)    
  abszdiff = ABS(zdiff)    

  Nsats = n_elements(xdiff)

;-- store signs of differences
  xsign = fltarr(Nsats) + 1.0
  whneg = where(xdiff lt 0)
  xsign[whneg] = -1.0
  ysign = fltarr(Nsats) + 1.0
  whneg = where(ydiff lt 0)
  ysign[whneg] = -1.0
  zsign = fltarr(Nsats) + 1.0
  whneg = where(zdiff lt 0)
  zsign[whneg] = -1.0

;-- correct the absolute value of the differences so that they cannot
;   exceed Lbox/2

  whx=where(absxdiff gt Lbox/2.)
  absxdiff[whx] = ABS(absxdiff[whx]-Lbox)

  why=where(absydiff gt Lbox/2.)
  absydiff[why] = ABS(absydiff[why]-Lbox)

  whz=where(abszdiff gt Lbox/2.)
  abszdiff[whz] = ABS(abszdiff[whz]-Lbox)

;--- now use stored signs of differences
  xdiff = absxdiff*xsign
  ydiff = absydiff*ysign
  zdiff = abszdiff*zsign

;--- scale all host-centric positions by Rvir
  Rvir = satellites.rvir/1000. ;<-- remember, .rvir tag is in kpc
  xdiff = xdiff/rvir
  ydiff = ydiff/rvir
  zdiff = zdiff/rvir

;--- plot simple histograms of xdiff, ydiff, and zdiff
;--- should all lie on top of one another, and look like a projected NFW
  window,0
  bplothist,xdiff,bin=0.01,xstyle=1,xrange=[-1.1,1.1]
  bplothist,ydiff,bin=0.01,xstyle=1,xrange=[-1.1,1.1],/overplot,color=!red
  bplothist,zdiff,bin=0.01,xstyle=1,xrange=[-1.1,1.1],/overplot,color=!blue

;--- visually examine y-z slice for angular patterns
  window,1
  plot,ydiff,zdiff,psym=3,xstyle=1,ystyle=1,xrange=[-1.1,1.1],yrange=[-1.1,1.1]

;--- visually examine x-z slice for angular patterns
  window,2
  plot,xdiff,zdiff,psym=3,xstyle=1,ystyle=1,xrange=[-1.1,1.1],yrange=[-1.1,1.1]



return
end

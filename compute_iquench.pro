pro compute_central_iquench,host_halos,logM,fquenchcen

  whcens = where(host_halos.Ncen eq 1,Ncens)
  cens = host_halos[whcens]
  central_iquench_randomizer=RandomU(seed,Ncens,/double)
  pcen_vector = INTERPOL(fquenchcen,logM,alog10(cens.Mvir))
  whquenchcen = where(central_iquench_randomizer lt pcen_vector,Nquenchcen)
  cens[whquenchcen].iquench = 1

  host_halos[whcens] = cens

return
end

;***************************
pro compute_satellite_iquench,satellites,logM,fquenchsat

  Nsats = n_elements(satellites.id)
  satellite_iquench_randomizer=RandomU(seed,nsats,/double)

  psat_vector = INTERPOL(fquenchsat,logM,alog10(satellites.Mvir))

  whquenchsat = where(satellite_iquench_randomizer lt psat_vector,Nquenchsat)
  satellites[whquenchsat].iquench = 1

return
end
;***************************
pro compute_satellite_quenchcen_iquench,satellites,logM,fquenchsat_quenchcen

  wh_quenchcen = where(satellites.iquenchcen eq 1,Nsats_quenchcen)
  satellites_quenchcen = satellites[wh_quenchcen]

  Nsats = n_elements(satellites_quenchcen.id)
  satellite_iquench_randomizer=RandomU(seed,nsats,/double)

  psat_vector = INTERPOL(fquenchsat_quenchcen,logM,alog10(satellites_quenchcen.Mvir))
;  print,minmax(psat_vector)

  whquenchsat = where(satellite_iquench_randomizer lt psat_vector,Nquenchsat)
  satellites_quenchcen[whquenchsat].iquench = 1

  satellites[wh_quenchcen] = satellites_quenchcen


return
end
;***************************
pro compute_satellite_activecen_iquench,satellites,logM,fquenchsat_activecen

  wh_activecen = where(satellites.iquenchcen eq 0,Nsats_activecen)
  satellites_activecen = satellites[wh_activecen]

  Nsats = n_elements(satellites_activecen.id)
  satellite_iquench_randomizer=RandomU(seed,nsats,/double)

  psat_vector = INTERPOL(fquenchsat_activecen,logM,alog10(satellites_activecen.Mvir))
;  print,minmax(psat_vector)

  whquenchsat = where(satellite_iquench_randomizer lt psat_vector,Nquenchsat)
  satellites_activecen[whquenchsat].iquench = 1

  satellites[wh_activecen] = satellites_activecen


return
end

;***************************

;*********************************************
function maximum_conformity_quenchsats_quenchcens,Nsat,quenchfrac_sat,quenchfrac_cen,dest_quenchcen

  maxconf_value = 1.
  Nquenchsat = quenchfrac_sat*Nsat

  IF (quenchfrac_sat gt 0 and quenchfrac_cen gt 0 and Nquenchsat gt 0) THEN BEGIN
;*** determine Ratio (eqn 18)
     ratio = quenchfrac_sat / (quenchfrac_cen*dest_quenchcen)
     print,' Cqq ratio = ',ratio
;*** compute maximum mean number of quench sats in halos with a quench cen (eqn 19)
     IF (ratio le 1) THEN max_Nquenchsat_quenchcen = quenchfrac_sat*Nsat / quenchfrac_cen
     IF (ratio gt 1) THEN max_Nquenchsat_quenchcen = dest_quenchcen*nsat
;*** convert to maximum conformity function (eqn 23)
     maxconf_value = max_Nquenchsat_quenchcen / (Nquenchsat*dest_quenchcen)
  ENDIF

return,maxconf_value
end
;*********************************************
function maximum_conformity_quenchsats_activecens,Nsat,quenchfrac_sat,activefrac_cen,dest_activecen

  maxconf_value = 1
  Nquenchsat = quenchfrac_sat*Nsat

  IF (quenchfrac_sat gt 0 and activefrac_cen gt 0 and Nquenchsat gt 0) THEN BEGIN
;*** determine Ratio (eqn 18 with quenchcen-->activecen)
     ratio = quenchfrac_sat / (activefrac_cen*dest_activecen)
     print,' Cqsf ratio = ',ratio
;*** compute maximum mean number of quench sats in halos with a active cen (eqn 22)
     IF (ratio le 1) THEN max_Nquenchsat_activecen = Nquenchsat / activefrac_cen
     IF (ratio gt 1) THEN max_Nquenchsat_activecen = dest_activecen*nsat
;*** convert max counts to maximum conformity function (eqn 23 with quenchcen-->activecen)
     maxconf_value = max_Nquenchsat_activecen / (Nquenchsat*dest_activecen)
  ENDIF

  return,maxconf_value
  end
;*********************************************
function minimum_conformity_quenchsats_quenchcens,max_Nquenchsats_activecens,Nsat,quenchfrac_sat,quenchfrac_cen,dest_quenchcen

  activefrac_cen = 1 - quenchfrac_cen
  Nquenchsat = Nsat*quenchfrac_sat
  dest_activecen = (1.0 - dest_quenchcen*quenchfrac_cen) / activefrac_cen

  IF (activefrac_cen gt 0 and dest_activecen gt 0) THEN BEGIN
;*** determine Ratio (eqn 18 with quenchcen-->activecen)
     ratio = quenchfrac_sat / (activefrac_cen*dest_activecen)
  ENDIF

  IF (ratio le 1) THEN min_Nquenchsat_quenchcen = 0.
  IF (ratio gt 1) THEN min_Nquenchsat_quenchcen = (Nquenchsat - activefrac_cen*max_Nquenchsats_activecens) / quenchfrac_cen

  IF (min_Nquenchsat_quenchcen lt 0) THEN min_Nquenchsat_quenchcen = 0.

  IF (Nquenchsat*dest_quenchcen) gt 0 THEN minconf_value = min_Nquenchsat_quenchcen / (Nquenchsat*dest_quenchcen)
  IF (Nquenchsat*dest_quenchcen) eq 0 THEN minconf_value = 0.0

  return,minconf_value
  end

;*********************************************

;*********************************************
function conformity_function_quenchsats_quenchcens,logM,low_logM,high_logM,Aconf,Bconf,Nsat,quenchfrac_sat,quenchfrac_cen,dest_quenchcen

  Nquenchsat = quenchfrac_sat*Nsat

;*** Compute baseline function value
  IF (logM le low_logM) THEN conformity_function_value = Aconf
  IF (logM ge high_logM) THEN conformity_function_value = Aconf + Bconf*(high_logM - low_logM)
  IF (logM gt low_logM and logM lt high_logM) THEN conformity_function_value = Aconf + Bconf*(logM - low_logM)

;*** Now impose max/min consistency conditions

  maxconf_quenchsat_quenchcen = maximum_conformity_quenchsats_quenchcens(Nsat,quenchfrac_sat,quenchfrac_cen,dest_quenchcen)

  activefrac_cen = 1.0 - quenchfrac_cen
  dest_activecen = (1.0 - dest_quenchcen*quenchfrac_cen) / activefrac_cen
  maxconf_quenchsat_activecen = maximum_conformity_quenchsats_activecens(Nsat,quenchfrac_sat,activefrac_cen,dest_activecen)
  max_Nquenchsat_activecen = maxconf_quenchsat_activecen*Nquenchsat*dest_activecen

  minconf_quenchsat_quenchcen = minimum_conformity_quenchsats_quenchcens(max_Nquenchsat_activecen,Nsat,quenchfrac_sat,quenchfrac_cen,dest_quenchcen)

  IF (conformity_function_value gt maxconf_quenchsat_quenchcen) THEN conformity_function_value = maxconf_quenchsat_quenchcen
  IF (conformity_function_value lt minconf_quenchsat_quenchcen) THEN conformity_function_value = minconf_quenchsat_quenchcen

return,conformity_function_value
end
;*********************************************
;*********************************************
function conformity_function_activesats_activecens,logM,low_logM,high_logM,Aconf,Bconf,Nsat,quenchfrac_sat,quenchfrac_cen,dest_quenchcen

  activefrac_sat = 1.0 - quenchfrac_sat
  activefrac_cen = 1.0 - quenchfrac_cen

  conformity_function_quenchsats_quenchcens = conformity_function_quenchsats_quenchcens(logM,low_logM,high_logM,Aconf,Bconf,Nsat,quenchfrac_sat,quenchfrac_cen,dest_quenchcen)

  conformity_function_activesats_quenchcens = (1.0 - quenchfrac_sat*conformity_function_quenchsats_quenchcens) / activefrac_sat

  conformity_function_activesats_activecens = (1.0 - quenchfrac_cen*conformity_function_activesats_quenchcens) / activefrac_cen

  conformity_function_value = conformity_function_activesats_activecens

return,conformity_function_value
end

;*********************************************



;*********************************************

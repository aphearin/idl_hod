function hodnsat,Mhost,M0,M1,alpha
;*** Functional form adopted (introduced?) in Zheng+04, arXiv:0408564

  nsat = ((Mhost-M0)/M1)^alpha

return,nsat
end

;**********

function hodncen,logM,logMmin,sigmaM

  erfarg = (logM-logMmin)/sigmaM
  ncen = 0.5*(1.0 + erf(erfarg))

return,ncen
end

;**********
function anatoly_concentration,masses
;*** simple z=0 c-M fitting formula from Anatoly's 2011 Bolshoi paper

  c0 = 12.0
  Mpiv = 1.e12
  a = -0.075

  concentrations = c0*(masses/Mpiv)^a
  
return,concentrations
end

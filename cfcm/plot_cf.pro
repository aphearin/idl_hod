.reset
@preplotwin.pro

filename='CF_DATA/all_galaxies.dat'
readcol,filename,dummy,r,dummy,cf,cferr

plot,r,cf,/xlog,/ylog,xstyle=1,ystyle=1,xrange=[0.1,25],yrange=[0.1,3000],$
xtitle=textoidl('r (Mpc/h)'),ytitle=textoidl('\xi'),title=textoidl('HOD Mock')
oploterror,r,cf,cferr,psym=3

filename='CF_DATA/quenched_galaxies.dat'
readcol,filename,dummy,r,dummy,cf,cferr
oplot,r,cf,color=!red
oploterror,r,cf,cferr,psym=3,color=!red


filename='CF_DATA/active_galaxies.dat'
readcol,filename,dummy,r,dummy,cf,cferr
oplot,r,cf,color=!blue
oploterror,r,cf,cferr,psym=3,color=!blue



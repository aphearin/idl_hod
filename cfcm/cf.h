CCCCCCCCCCCCCCCCCCCCCCCCCC
C
C particle position & weight information
C
CCCCCCCCCCCCCCCCCCCCCCCCCC

      integer Nmaxpart, NDIM
      parameter ( Nmaxpart = 200000 , NDIM = 3 )

      integer octant( Nmaxpart ), nptcl
      real*8 xyzp( NDIM , Nmaxpart ), wp( Nmaxpart ), LBOXH
      real*8 wpmean, wp2mean

      parameter( LBOXH = 250.0 ) ! box size in h^-1 Mpc co-moving.

      common /PARTICLES/ xyzp, wp, wpmean, wp2mean, octant, nptcl
CCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C Particle Chaining Mesh
C
CCCCCCCCCCCCCCCCCCCCCCCCCCC

      integer Ngridmax, Ngrid
      parameter ( Ngridmax=32 )

      integer pcell( Ngridmax, Ngridmax, Ngridmax )
      integer llp( Nmaxpart )

      real*8 Lcell

      COMMON / CHAIN_MESH / Lcell, pcell, llp, Ngrid
CCCCCCCCCCCCCCCCCCCCCCCCCCC

CCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C   pair counts
C
CCCCCCCCCCCCCCCCCCCCCCCCCCC

      integer Nmaxbin ! maximum number of separation bins !
      parameter ( Nmaxbin = 30 )

C--------------------
c number of pairs and number of pairs weighted appropriately
C--------------------
      real*8 npairs( 9 , Nmaxbin )
      real*8 nwpairs( 9 , Nmaxbin )
      
      common /PAIRCOUNT/ npairs, nwpairs

CCCCCCCCCCCCCCCCCCCCCCCCCCC

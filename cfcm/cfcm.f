CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C     Compute a 3D real-space correlation function 
C     of objects in the input file.
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      program cf

      implicit none

      include 'cf.h'
C===========================

      call cf_driver()
      
      END
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C     driver for cf
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine cf_driver()

      implicit none

      include 'cf.h'

      character*256 infile
      character*256 outfile

      integer outunit, ioflag
      parameter( outunit = 90 )

      character*16 chtemp

      real*8 rlow, rhigh

      integer nbins

      integer iargc
      external iargc

C======================

C----------------------
C     check arguments, if ok read in args.
C----------------------

      call getarg(1,infile)
      call getarg(2,outfile)

      call getarg(3,chtemp)
      read(chtemp,*) rlow
      
      call getarg(4,chtemp)
      read(chtemp,*) rhigh

      call getarg(5,chtemp)
      read(chtemp,*) nbins

C----------------------
C     read in particle data.
C----------------------

      call get_xyz( infile )

C----------------------
C     get xi(r)
C----------------------

      call openoutput( outfile , outunit )

      call get_cf( rlow, rhigh, nbins, outunit )
      
      close( outunit )

      RETURN
      END
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C     get correlation function, 
C     computes weighted and unweighted 
C     1+xi(r) and errors.
C
C     assumes particle data already read into memory.
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine get_cf( rlow, rhigh, Nbins, ounit )

      implicit none

      real*8 pi4on3
      parameter ( pi4on3 = 4.188790205 )

      integer Nbins, lcv, lcvoct, ounit, lcv1, lcv2

      real*8 rinbin( Nbins ), randprob( Nbins )
      real*8 onepxi( 9, Nbins ), erronepxi( Nbins )
      real*8 onepxiw( 9, Nbins ), erronepxiw( Nbins )
      real*8 xidum, xiwdum, errxidum, errxiwdum

      real*8 rlow, rhigh
      real*8 lgrlow, lgrhigh, dlgr

      real*8 rleft, rright, dV

      real*8 rand, wrand

      real*8 rt, wrt, poisseb, npreal

      include 'cf.h'

C====================================================

      npreal = float( nptcl )

      poisseb = sqrt( npreal )

      rand = ( npreal * ( npreal - 1) / 2. )/( LBOXH**3 )

      wrand = wp2mean * 
     & ( npreal * (npreal - 1 ) / 2. )/( LBOXH**3 ) 

      lgrlow = log10(rlow)
      lgrhigh = log10(rhigh)
      dlgr = (lgrhigh - lgrlow) / float( Nbins )

C-----------------
C     count all pairs
C-----------------

      call count_pairs( rlow, rhigh, Nbins, rinbin )

C-----------------
C     xi(r) for box
C-----------------

      do lcv = 1, Nbins

         rright = lgrlow + float(lcv)*dlgr
         rleft = rright - dlgr
         rright = 10.0**rright
         rleft = 10.0**rleft
         
         dV = pi4on3*( rright**3 - rleft**3 )

         rt = rand*dV
         wrt = wrand*dV

         randprob(lcv) = rt

         xidum = REAL( npairs(1,lcv) )/rt - 1.
         xiwdum = nwpairs(1,lcv)/wrt - 1.

         poisseb = sqrt( npairs( 1, lcv ) ) !> poisson errorbar weight <!
         
         rt = 7.*rt/8.
         wrt = 7.*wrt/8.
c
c         print*,' '
c         print*,' r = ',rleft,' to ',rright
c         print*,' rt = ',rt
c         print*,' npairs = ',npairs(1,lcv)
c         print*,' xi = ',xidum
c
         
C---- get jackknife error

         errxidum = 0.
         errxiwdum = 0.

         do lcvoct = 2,9

            onepxi( lcvoct, lcv ) = 
     & REAL( npairs( lcvoct, lcv ) )/rt - 1.

            errxidum = errxidum + 
     & ( onepxi( lcvoct , lcv ) - xidum )**2

            onepxiw( lcvoct, lcv ) = 
     &  nwpairs( lcvoct, lcv )/wrt - 1.

            errxiwdum = errxiwdum + 
     & ( onepxiw( lcvoct, lcv ) - xiwdum)**2

         enddo

         if (npairs(1,lcv).gt.0.D0) then
            rinbin(lcv) = rinbin(lcv)/REAL( npairs(1,lcv) )
         else
            rinbin(lcv) = 0.
         endif

C------------------------
C     Set error = jackknife error plus poisson error
C------------------------

         errxidum = sqrt( 8.*errxidum/7. ) ! + (1. + xidum)/poisseb

         errxiwdum = sqrt( 8.*errxiwdum/7. ) ! + (1. + xiwdum)/poisseb

         onepxi(1, lcv) = xidum !+ 1. output xi(r) rather than 1+xi(r)
         erronepxi(lcv) = errxidum

         onepxiw(1, lcv) = xiwdum !+ 1. output xi(r)
         erronepxiw(lcv) = errxiwdum

      enddo

C-------------------------
C     Calculation complete now output.
C-------------------------

      do lcv=1,Nbins
         
         if (rinbin(lcv).gt.0.) then

            rright = lgrlow + float(lcv)*dlgr
            rleft = rright-dlgr
            rright = 10.0**rright
            rleft = 10.0**rleft
            
            write( ounit, 901 )
     & rleft, rinbin(lcv), rright, onepxi( 1, lcv ), erronepxi( lcv ), 
     & ( onepxi( lcvoct, lcv ), lcvoct=2,9 ), 
     & ( npairs( lcv1 , lcv ), lcv1=1,9 ), randprob( lcv )
c
c onepxiw( 1, lcv), 
c     & erronepxiw( lcv ) 

         endif

      enddo

 901  format(1X,3(F9.4,2X),1P,20(E12.5,2X))

      RETURN
      END
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C     count pairs
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine count_pairs( rlow, rhigh, Nbins, rinbin )

      implicit none

      include 'cf.h'

      integer ixcen, iycen, izcen
      integer ixstart, iystart, izstart
      integer ixend, iyend, izend
      integer ixnear, iynear, iznear
      integer ix, iy, iz, di

      integer lcvp1, lcvp2, lcvoct

      integer Nbins, binnumber, poct 

      real*8 rinbin( Nbins )

      real*8 rlow, rhigh, separation, lgsep
      real*8 lgRlow, lgRhigh, dlgR

      real*8 dist2pts
      external dist2pts
C========================

C-----------------------
C      initialize
C-----------------------

      lgRlow = log10(rlow)
      lgRhigh = log10(rhigh)

      dlgR = ( lgRhigh - lgRlow ) / float( Nbins )
      
      call init_array( rinbin , Nbins )
      call init_2D_array( npairs, 9, Nmaxbin )
      call init_2D_array( nwpairs, 9, Nmaxbin )

C-----------------------
C     count pairs
C-----------------------

      di = int(rhigh/Lcell)+1 ! number of neighboring cells to consider

C      PRINT*,' ......Ngrid = ',Ngrid,'   Lcell = ',Lcell
C      PRINT*,' ......Span ',di,' cells of mesh '
C      PRINT*,' '

C...... loop over central mesh cells
      
      DO ixcen=1,Ngrid
         DO iycen=1,Ngrid
            DO izcen=1,Ngrid

               ixstart = ixcen - di
               ixend = ixcen + di
               
               iystart = iycen - di
               iyend = iycen + di
               
               izstart = izcen - di
               izend = izcen + di
               
C...... assign central particle

               lcvp1=pcell(ixcen,iycen,izcen)
c
c               PRINT*,' ixcen = ',ixcen
c               PRINT*,' ixstart = ',ixstart,'  ixend = ',ixend
c               PRINT*,' iycen = ',iycen
c               PRINT*,' iystart = ',iystart,'  iyend = ',iyend
c               PRINT*,' izcen = ',izcen
c               PRINT*,' izstart = ',izstart,'  izend = ',izend
c               PRINT*,' lcvp1 = ',lcvp1
c
               DO WHILE ( lcvp1.gt.0 )

C......... loop over neighboring mesh cells
               
                  DO ixnear=ixstart,ixend
                     DO iynear=iystart,iyend
                        DO iznear=izstart,izend
                        
                           IF ( ixnear.lt.1 ) THEN
                              ix=Ngrid+ixnear
                           ELSEIF ( ixnear.gt.Ngrid ) THEN 
                              ix=ixnear-Ngrid
                           ELSE
                              ix=ixnear
                           ENDIF

                           IF ( iynear.lt.1 ) THEN 
                              iy=Ngrid+iynear
                           ELSEIF ( iynear.gt.Ngrid ) THEN 
                              iy=iynear-Ngrid
                           ELSE
                              iy=iynear
                           ENDIF

                           IF ( iznear.lt.1 ) THEN
                              iz=Ngrid+iznear
                           ELSEIF ( iznear.gt.Ngrid ) THEN 
                              iz=iznear-Ngrid
                           ELSE
                              iz=iznear
                           ENDIF


                           IF ( (ix.eq.ixcen)
     &                             .and.
     &                          (iy.eq.iycen)
     &                             .and.
     &                          (iz.eq.izcen) ) THEN
                              lcvp2=llp(lcvp1)
                           ELSE
                              lcvp2=pcell(ix,iy,iz)
                           ENDIF

c
c                           PRINT*,' lcvp1 = ',lcvp1
c                           PRINT*,' lcvp2 = ',lcvp2
c                           PRINT*,' '
c                           PRINT*,' ixcen = ',ixcen
c                           PRINT*,' iycen = ',iycen
c                           PRINT*,' izcen = ',izcen
c                           PRINT*,' '
c                           PRINT*,' ix = ',ix
c                           PRINT*,' iy = ',iy
c                           PRINT*,' iz = ',iz
c                           PRINT*,' '
c                           PAUSE
c

                           DO WHILE ( lcvp2.gt.0 ) 
                              
c                              PRINT*,' lcvp2 = ',lcvp2
                              
C-----------------------
C     Get separation
C-----------------------

                              separation = dist2pts( 
     & xyzp(1,lcvp1), xyzp(2,lcvp1), xyzp(3,lcvp1), 
     & xyzp(1,lcvp2), xyzp(2,lcvp2), xyzp(3,lcvp2), 
     & LBOXH )

C-----------------------
C     If in range bin it up.
C-----------------------
                              if ((separation.lt.rhigh)
     &                             .and.
     &                             (separation.ge.rlow)) then
            
                                 lgsep = log10( separation )

                                 binnumber = max( 1, 
     & min( Nbins, (int( (lgsep - lgRlow) / dlgR ) + 1 ) ) )

                                 npairs( 1, binnumber ) = 
     & npairs( 1, binnumber ) + 1.D0

                                 nwpairs( 1, binnumber ) = 
     & nwpairs( 1, binnumber ) + wp(lcvp1)*wp(lcvp2)

                                 rinbin( binnumber ) = 
     & rinbin( binnumber ) + separation

c                                 IF ( binnumber.eq.Nbins ) THEN
c                                    PRINT*,' lcvp1 = ',lcvp1
c                                    PRINT*,' lcvp2 = ',lcvp2
c                                    PRINT*,' separation = ',separation
c                                    PRINT*,' lgsep = ',lgsep
c                                    PAUSE
c                                 ENDIF

C-----------------------
C     Add to octant pairs if neither are in the 
C     excluded octant.
C-----------------------
                                 DO lcvoct=1,8

                                    IF ( (octant(lcvp1).ne.lcvoct)
     &                                          .and.
     &                               (octant(lcvp2).ne.lcvoct) ) THEN

                                       poct = 1 + lcvoct

                                       npairs( poct , binnumber ) = 
     & npairs( poct , binnumber ) + 1.D0
                                       nwpairs( poct , binnumber ) = 
     & nwpairs( poct , binnumber ) + wp(lcvp1)*wp(lcvp2)

                                    ELSEIF ( (octant(lcvp1).ne.lcvoct)
     &                                             .or.
     &                                (octant(lcvp2).ne.lcvoct) ) THEN

                                       poct = 1 + lcvoct
                                       
                                       npairs( poct, binnumber ) = 
     & npairs( poct, binnumber ) + 0.5D0
                                       nwpairs( poct, binnumber ) = 
     & nwpairs( poct, binnumber ) + 0.5*wp(lcvp1)*wp(lcvp2)

                                    ENDIF

                                 ENDDO

                              endif

                              lcvp2 = llp(lcvp2) ! update current particle

                           ENDDO

                        ENDDO
                     ENDDO
                  ENDDO

                  lcvp1=llp(lcvp1)

               ENDDO

               pcell(ixcen,iycen,izcen)=-1
            ENDDO
         ENDDO
      ENDDO

      RETURN
      END
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C     initialize particle positions and octant labels.
C     build a chaining mesh for particle locations.
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine get_xyz( dfile )
      
      implicit none

      include 'cf.h'
      
      character*256 dfile
      integer dunit, ioflag, lcv, ix, iy, iz, lcvdim, poct
      parameter( dunit = 10 )

      real*8 xread, yread, zread, wread, boxon2, wsum, w2sum
C==============================

C-- Initialize chaining mesh - not adaptive
      Ngrid=Ngridmax

      Lcell=LBOXH/FLOAT( Ngrid ) ! mesh cell size in 1D

      DO ix=1,Ngrid
         DO iy=1,Ngrid
            DO iz=1,Ngrid
               pcell(ix,iy,iz)=0
            ENDDO
         ENDDO
      ENDDO

      call opendata( dfile , dunit )

      ioflag = 0
      lcv = 0
      wsum = 0.
      w2sum = 0.

      boxon2 = LBOXH / 2.

      do while (ioflag.eq.0) 

         read(dunit,*,iostat=ioflag,END=1099)
     & xread, yread, zread
         wread = 1.d0

         if (ioflag.eq.0) then
            
            lcv = lcv + 1
c... assign position
            xyzp(1,lcv) = xread
            xyzp(2,lcv) = yread
            xyzp(3,lcv) = zread
            wp(lcv) = wread

            wsum = wsum + wread
            w2sum = w2sum + wread**2

c... assign octant
            poct = 1

            do lcvdim = 1 , NDIM
               if (xyzp(lcvdim,lcv).le.boxon2) 
     &              poct = poct + 2**( lcvdim - 1 )
            enddo

            octant(lcv) = poct

C... Incorporate into chaining mesh

            ix = INT( xyzp(1,lcv)/Lcell ) + 1
            iy = INT( xyzp(2,lcv)/Lcell ) + 1
            iz = INT( xyzp(3,lcv)/Lcell ) + 1
            
c            IF ( (xread.lt.Lcell).and.(yread.lt.Lcell) ) THEN
c               PRINT*,' lcv = ',lcv
c               PRINT*,' x = ',xread
c               PRINT*,' y = ',yread
c               PRINT*,' z = ',zread
c               PRINT*,' ix = ',ix
c               PRINT*,' iy = ',iy
c               PRINT*,' iz = ',iz
c               PAUSE
c            ENDIF

            llp(lcv)=pcell(ix,iy,iz)
            pcell(ix,iy,iz)=lcv
            
         endif         

      enddo
 1099 CONTINUE
      CLOSE(dunit)

      nptcl = lcv
      wpmean = wsum / float( nptcl )
      wp2mean = w2sum / float( nptcl )

      print*,' '
      print*,'    DATA READ COMPLETE, Number of particles = ',lcv
      print*,' '

      RETURN
      END
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine opendata(dfile,dunit)

      implicit none

      character*256 dfile
      integer dunit, ioflag, cpos

      logical opflag
C=========================================

      inquire(unit=dunit,opened=opflag)

      if (opflag) then
         print*,' '
         print*,' ERROR: This unit is already '
         print*,'        opened.'
         print*,'   unit = ',dunit,' '
         print*,'   '
         STOP
      endif
C...
      
      cpos = index(dfile,' ')-1

      open( unit=dunit,file=dfile(1:cpos),
     & status='old',form='formatted',
     & iostat=ioflag )
      
      if (ioflag.ne.0) then

         print*,' '
         print*,' ERROR: Could not open data file.'
         print*,' '
         print*,'      file = '//dfile(1:cpos)//' '
         print*,'      unit = ',dunit
         print*,' '
         STOP
         
      endif

      RETURN
      END
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine openoutput(ofile,ounit)

      implicit none

      character*256 ofile
      integer ounit, ioflag, cpos

      logical opflag
C=============================================

      inquire(unit=ounit,opened=opflag)

      if (opflag) then
         print*,' '
         print*,' ERROR: This unit is already opened '
         print*,' '
         print*,'       unit = ',ounit
         print*,' '
         STOP
      endif
C...

      cpos = index(ofile,' ')-1

      open(unit=ounit,file=ofile(1:cpos),
     & status='unknown',form='formatted',
     & iostat=ioflag)
      
      if (ioflag.ne.0) then
         print*,' '
         print*,' ERROR: Could not open data file.'
         print*,' '
         print*,'      file = '//ofile(1:cpos)//' '
         print*,'      unit = ',ounit
         print*,' '
         STOP
      endif

      RETURN
      END
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C     distance between points 1 and 2 in the box.
C     box the box size in the same units 
C     as the x,y,z values.  
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      function dist2pts(x1,y1,z1,x2,y2,z2,box)

      implicit none

      real*8 dist2pts, x1, y1, z1, x2, y2, z2, boxon2
      real*8 box

      real*8 dx, dy, dz, shx, shy, shz
C=====================================================
C
      boxon2=box/2.

      dx = x1-x2
      dy = y1-y2
      dz = z1-z2

      if (dx.ge.boxon2) then
         shx = -box
      elseif (dx.lt.-boxon2) then
         shx = box
      else
         shx = 0.
      endif
      
      if (dy.ge.boxon2) then
         shy = -box
      elseif (dy.lt.-boxon2) then
         shy = box
      else
         shy = 0.
      endif

      if (dz.ge.boxon2) then
         shz = -box
      elseif (dz.lt.-boxon2) then
         shz = box
      else
         shz = 0.
      endif

      dist2pts = sqrt( (dx + shx)**2 + 
     & (dy + shy)**2 + (dz + shz)**2 )

      RETURN
      END
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine init_array( vec , nvec )
      implicit none
      
      integer nvec, lcv
      real*8 vec( nvec )

      do lcv=1,nvec
         vec(lcv) = 0.
      enddo
      RETURN
      END
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine init_2D_array( vec , n1, n2 )

      implicit none
      
      integer n1, n2, lcv1, lcv2
      real*8 vec( n1, n2 )

      do lcv1=1,n1
         do lcv2=1,n2
            vec(lcv1,lcv2) = 0.
         enddo
      enddo

      RETURN
      END
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

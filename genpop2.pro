; Simple routine to generate a population representing an input PDF
; x is a data vector of abcissa values at which the input PDF is evaluated
; y is a vector of values of the PDF function evaluated at each point in x
; y need not be normalized: only the *relative* x-abundances need to be correct
;
; Note that the range of values over which the PDF is sampled is determined by x
; Ditto, the dimension of x specifies how finely the PDF is sampled
; Ndraws specifies the number of times to draw from the input PDF
;
;*** I wrote this routine from scratch
;*** It's a simple sample and keep/reject loop
;*** and it is horribly slow
;*** There MUST be faster, more intelligent routines for population generation
;
function genpop2,x,y,Ndraws
  t1=systime(1,/seconds)

  vmin=min(x)
  vmax=max(x)
  Ndata = n_elements(x)
  normalization = TOTAL(y)
  y = y/normalization

  draws = fltarr(Ndraws)

  FOR ii=0LL,Ndraws-1 DO BEGIN
     endloop=0

     WHILE (endloop eq 0) DO BEGIN
        draw = RandomU(seed1,/double)*(vmax-vmin) + vmin
        pdraw = INTERPOL(y,x,draw)
        pkeep = RandomU(seed2)
;
        IF (pdraw ge pkeep) THEN BEGIN
           draws[ii] = draw
           endloop=1
        ENDIF
;
     ENDWHILE

  ENDFOR
  
  t2=systime(1,/seconds)
;  print,'Total loop time = ',(t2-t1),' seconds'

return,draws
end

pro output_hod_mock,centrals,satellites,filename

  Ncentrals = n_elements(centrals.id)
  Nsatellites = n_elements(satellites.id)

  OPENW,1,filename
  FOR ii=0LL,Ncentrals-1 DO PRINTF,1,centrals[ii].pos[0],centrals[ii].pos[1],centrals[ii].pos[2],centrals[ii].vel[0],centrals[ii].vel[1],centrals[ii].vel[2],FORMAT='(6(F10.3,2X))'
  FOR ii=0LL,Nsatellites-1 DO PRINTF,1,satellites[ii].pos[0],satellites[ii].pos[1],satellites[ii].pos[2],satellites[ii].vel[0],satellites[ii].vel[1],satellites[ii].vel[2],FORMAT='(6(F10.3,2X))'
  CLOSE,1

return
end

;**********************

pro output_hod_mock_posonly,centrals,satellites,filename

  Ncentrals = n_elements(centrals.id)
  Nsatellites = n_elements(satellites.id)

  OPENW,1,filename
  FOR ii=0LL,Ncentrals-1 DO PRINTF,1,centrals[ii].pos[0],centrals[ii].pos[1],centrals[ii].pos[2],FORMAT='(3(F10.3,2X))'
  FOR ii=0LL,Nsatellites-1 DO PRINTF,1,satellites[ii].pos[0],satellites[ii].pos[1],satellites[ii].pos[2],FORMAT='(3(F10.3,2X))'
  CLOSE,1

return
end
;**********************

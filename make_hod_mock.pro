.reset
.run occupation_statistics.pro
.run output_hod_mock.pro
.run nfw.pro
.run create_satellite_data_structure.pro
.run genpop2.pro
.run INTERPOL.pro
.run distances_in_periodic_box.pro
.run add_tags.pro
.run listmatch.pro
.run readcol.pro
.run remchar.pro
.run gettok.pro
.run strnumber.pro
;
.run quenching_model.pro
.run compute_iquench.pro
.run conformity_functions.pro
.run load_fiducial_quenching_inputs.pro
.run assign_host_quenching_to_satellites.pro

restore,'/Users/aphearin/Dropbox/make_HOD_mock_IDL/DATA/Bolshoi_host_halos_a1.0003.sav'
Lbox = 250.0
Nhosts = n_elements(host_halos.Mvir)
t_init=systime(1,/seconds)
;
$clear
print,''

;************************************************************************
;****************** Load HOD parameters for baseline threshold sample ******************

@threshold_params.dat

;*** Assign central galaxies to host halos
$clear
print,''
print,'...Assigning central galaxies to host halos'
print,''

;--- Enforce host halo concentrations to obey Anatoly's mean relation
host_halos.conc = anatoly_concentration(host_halos.Mvir)
;
Ncenray = hodNcen(alog10(host_halos.Mvir),logMmin,sigma)
randomray = RandomU(seed,Nhosts)
wh_cenyes = where(randomray lt Ncenray,Ncens)
host_halos[wh_cenyes].Ncen = 1
centrals = host_halos[wh_cenyes]
;**********************************
;--- assign colors to centrals
;--- Read fiducial model parameters and construct necessary arrays
load_fiducial_quenching_inputs,centrals,Factivecen,Factivesat,logMbins
Fquenchcen = 1.0 - Factivecen
Fquenchsat = 1.0 - Factivesat
compute_central_iquench,host_halos,logMbins,Fquenchcen

;load_conformity_inputs,Factivecen,Factivesat,logMbins,Nsat,Fquenchsat_quenchcen,Factivesat_activecen
;Fquenchsat_activecen = 1.0 - Factivesat_activecen 
;Factivesat_quenchcen = 1.0 - Fquenchsat_quenchcen

;**********************************
;
;*** Compute and assign number of satellites to each host halo
;print,'...Assigning Nsat to host halos'
host_halos.meanNsat = hodnsat(host_halos.Mvir,M0,M1,alpha)
wh_pos=where(host_halos.meanNsat gt 0,Nhosts_to_assign_satellite_count,complement=wh_nonpos)
host_halos[wh_nonpos].meanNsat = 0.
;
FOR ii=0LL,Nhosts_to_assign_satellite_count-1 DO host_halos[wh_pos[ii]].Nsat = RandomN(seed,Poisson=host_halos[wh_pos[ii]].meanNsat)
centrals = host_halos[wh_cenyes]

Ncen_total = TOTAL(host_halos.Ncen)
Nsat_total = TOTAL(host_halos.Nsat)
Ngal_total =  Ncen_total + Nsat_total
print,''
print,'M1/Mmin = ',String(M1/(10.^logMmin),'(F6.2)')
print,''
print,'Number density of sample = ',String(Ngal_total/double(Lbox^3.),'(F7.3)'),' (h/Mpc)^3'
print,''
print,'Satellite fraction = ',String(Nsat_total/double(Ncen_total),'(F6.3)')
print,''
wh_nohost = where(host_halos.Ncen eq 0 and host_halos.Nsat gt 0,ct_nohost)
print,'Number of satellites without hosts = ',String(ct_nohost,'(I4)')
print,''

;*** Create mock satellites
create_satellite_data_structure,host_halos,satellites
assign_host_properties_to_satellites,host_halos,satellites

print,'...Computing satellite positions'
;print,'( This step is needlessly slow and takes up virtually all the computation time )'
;*** Assign radii to satellites
tsat0=systime(1,/seconds)
satellites.radialpos = -1
new_fast_assign_radialpos_to_satellites,satellites,Fconc
;assign_radialpos_to_satellites,satellites,Fconc
tsat1=systime(1,/seconds)
;print,'DONE!'
;print,'Total runtime to draw satellite positions from an NFW distribution: ',String(tsat1-tsat0,'(F4.1)'),' seconds'
;print,''
;;*** Convert radii into host-centric positions
assign_spatial_positions_to_satellites,satellites,Lbox
;;*** Assign velocities to satellites
assign_velocities_to_satellites,satellites

;************************************************************************
;*******  Now assign quenched/active designations to satellites *******
;--- initialize satellite quenching designation
satellites.iquench = 0
satellites.iquenchcen = 0
;whcens = where(host_halos.Ncen gt 0)
;centrals = host_halos[whcens]
print,''
;
;********************************
;
;********************************
;--- Make fiducial color HOD mock
;compute_central_iquench,host_halos,logMbins,fquenchcen
;whcens = where(host_halos.Ncen gt 0)
;centrals = host_halos[whcens]
;
compute_satellite_iquench,satellites,logMbins,fquenchsat
assign_host_quenching_to_satellites,host_halos,satellites
print,''
print,'Done creating fiducial color HOD mock'
print,''
t_final=systime(1,/seconds)
print,String(t_final-t_init,'(F4.1)'),' total seconds to create the mock'
print,''
print,''
;********************************


;@preplotwin.pro
;bplothist,satellites.radialpos,bin=0.01,xstyle=1






;*** output ASCII data for correlation function calculations
;whqcens = where(centrals.iquench eq 1,complement=whacens)
;quenched_cens = centrals[whqcens]
;active_cens = centrals[whacens]

;whqsats = where(satellites.iquench eq 1,complement=whasats)
;quenched_sats = satellites[whqsats]
;active_sats = satellites[whasats]


;output_hod_mock_posonly,centrals,satellites,'SAMPLES/all_galaxies.dat'
;output_hod_mock_posonly,quenched_cens,quenched_sats,'SAMPLES/quenched_galaxies.dat'
;output_hod_mock_posonly,active_cens,active_sats,'SAMPLES/active_galaxies.dat'






;print,minmax(satellites.radialpos)
;wh=where(satellites.radialpos eq 0.0,ct)
;print,ct/double(n_elements(satellites.radialpos)),' satellites have zero host-centric position'
;
;whtest=where(satellites.hostconc ge 8 and satellites.hostconc le 10)
;stest=satellites[whtest]
;medconc = median(stest.hostconc)
;create_NFW_lookup_table,medconc,logradius_table,logNFW_cumulative_PDF_table

;Ngals=100000
;draw_NFW_radial_positions,logradius_table,logNFW_cumulative_PDF_table,Ngals,radial_positions


































;*** output the mock



;print,'...Writing output data'
;print,''
;;
;char_sigma = '_sigma'+String(sigma,'(F4.2)')
;char_logMmin = '_Mmin'+String(logMmin,'(F5.2)')
;char_alpha = '_alpha'+String(alpha,'(F4.2)')
;char_logM1 = '_M1'+String(logM1,'(F5.2)')
;char_logM0 = '_M0'+String(logM0,'(F5.2)')
;char_Fconc = '_Fconc'+String(Fconc,'(F4.2)')
;
;ASCII_output_filename = 'SAMPLES/mock_HOD'+char_sigma+char_logMmin+char_alpha+char_logM1+char_logM0+char_Fconc+'.dat'
;output_hod_mock,centrals,satellites,ASCII_output_filename

;
;IDL_output_filename = 'DATA/mock_HOD_hosts'+char_sigma+char_logMmin+char_alpha+char_logM1+char_logM0+char_Fconc+'.sav'
;save,host_halos,satellites,file=IDL_output_filename

;print,''
;print,'Done!'
;print,''

















pro load_conformity_inputs,Factivecen,Factivesat,logMbins,Nsat,Fquenchsat_quenchcen,Factivesat_activecen

  Npts = n_elements(logMbins)
  Fquenchcen = 1.0 - Factivecen
  Fquenchsat = 1.0 - Factivesat
  dest_quenchcen = fltarr(Npts) + 1.0
  dest_activecen = fltarr(Npts) + 1.0
;
  readcol,'conformity_inputs.dat',a,b,c,d,comment='#',FORMAT='F,F,F,F'
  Aconf = a[0]
  Bconf = b[0]
  low_logM = c[0]
  high_logM = d[0]

  Fquenchsat_quenchcen = fltarr(Npts)
  conf_quenchsat_quenchcen = fltarr(Npts)
  FOR ii=0,npts-1 DO BEGIN
     conf_quenchsat_quenchcen[ii] = conformity_function_quenchsats_quenchcens(logMbins[ii],low_logM,high_logM,Aconf,Bconf,Nsat[ii],Fquenchsat[ii],Fquenchcen[ii],dest_quenchcen[ii])
     Fquenchsat_quenchcen[ii] = Fquenchsat[ii]*conf_quenchsat_quenchcen[ii]
  ENDFOR

  Factivesat_activecen = fltarr(Npts)
  conf_activesat_activecen = fltarr(Npts)
  FOR ii=0,npts-1 DO BEGIN
     conf_activesat_activecen[ii] = conformity_function_activesats_activecens(logMbins[ii],low_logM,high_logM,Aconf,Bconf,Nsat[ii],Fquenchsat[ii],Fquenchcen[ii],dest_quenchcen[ii])
     Factivesat_activecen[ii] = Factivesat[ii]*conf_activesat_activecen[ii]
  ENDFOR

  Fquenchsat_activecen = 1.0 - Factivesat_activecen

;*** sanity check print statement
  FOR ii=0,Npts-1 DO print,logMbins[ii],Fquenchsat_activecen[ii],Fquenchsat[ii],Fquenchsat_quenchcen[ii]

return
end

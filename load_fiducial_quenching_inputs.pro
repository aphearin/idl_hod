pro load_fiducial_quenching_inputs,centrals,Factivecen,Factivesat,logMbins

  readcol,'fiducial_quenching_inputs.dat',a,b,c,d,comment='#',FORMAT='F,F,F,F',/silent
  frac_logM11_allcens = a[0]
  frac_logM15_allcens = b[0]
  frac_logM11_allsats = c[0]
  frac_logM15_allsats = d[0]
;
;*** Determine Mhost binning scheme
  logMmin = min(alog10(centrals.Mvir))
  logMmax = max(alog10(centrals.Mvir))
  dlogM = 0.15
  npts = ROUND((logMmax-logMmin)/dlogM)
  logMbins = findgen(npts)*(logMmax-logMmin)/double(npts-1) + logMmin
  logMbins[0] = logMbins[0] - 0.01
  logMbins[-1] = logMbins[-1] + 0.01
  Mbins = 10.^logMbins
;
  Factivecen = fltarr(Npts)
  FOR ii=0,npts-1 DO Factivecen[ii] = active_fraction_allcens(frac_logM11_allcens,frac_logM15_allcens,logMbins[ii])
;
  Factivesat = fltarr(Npts)
  FOR ii=0,npts-1 DO Factivesat[ii] = active_fraction_allsats(frac_logM11_allsats,frac_logM15_allsats,logMbins[ii])
;

return
end

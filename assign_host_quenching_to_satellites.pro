pro assign_host_quenching_to_satellites,host_halos,satellites
  
  Nsats = n_elements(satellites.ID)
  satellites.ID = findgen(Nsats)
  wh_sort = sort(satellites.hostid)
  satellites = satellites[wh_sort]
  wh_uniq = UNIQ(satellites.hostID)
  Nhosts = n_elements(wh_uniq)
  representative_sats = satellites[wh_uniq]
;
  listmatch,representative_sats.hostid,host_halos.ID,A_ind,B_ind
  representative_sats[A_ind].iquenchcen = host_halos[B_ind].iquench
  satellites[wh_uniq] = representative_sats

;*** start with the first host system
  first_index=0
  last_index=wh_uniq[0]
  satellites[first_index:last_index].iquenchcen = satellites[last_index].iquenchcen

;*** loop over remaining systems
  FOR ii=1LL,Nhosts-1 DO BEGIN
     first_index=last_index+1
     last_index=wh_uniq[ii]
     satellites[first_index:last_index].iquenchcen = satellites[last_index].iquenchcen
  ENDFOR

return
end
;***********************************
pro assign_host_quenching_to_HOD_satellites,hod
;  
  hod.iquenchcen = -1
;
  whsat = where(hod.icen eq 0,complement=whcen)
  satellites = hod[whsat]
  centrals = hod[whcen]
  centrals.iquenchcen = centrals.iquench
;
  Nsats = n_elements(satellites.ID)
;
  wh_sort = sort(satellites.hostid)
  satellites = satellites[wh_sort]
  wh_uniq = UNIQ(satellites.hostID)
  Nhosts = n_elements(wh_uniq)
  representative_sats = satellites[wh_uniq]
;
  listmatch,representative_sats.hostid,hod.ID,A_ind,B_ind
  representative_sats[A_ind].iquenchcen = hod[B_ind].iquench
  satellites[wh_uniq] = representative_sats

;*** start with the first host system
  first_index=0
  last_index=wh_uniq[0]
  satellites[first_index:last_index].iquenchcen = satellites[last_index].iquenchcen

;*** loop over remaining systems
  FOR ii=1LL,Nhosts-1 DO BEGIN
     first_index=last_index+1
     last_index=wh_uniq[ii]
     satellites[first_index:last_index].iquenchcen = satellites[last_index].iquenchcen
  ENDFOR

  hod[whsat] = satellites
  hod[whcen] = centrals


  wh_nomatch = where(hod.iquenchcen eq -1,Num_nomatch)
  print,''
  print,Num_nomatch,' objects have no match'
  print,'Assigning iquenchcen tag using nearest neighbor color for these objects'
  nomatch = hod[wh_nomatch]

  FOR ii=0L,Num_nomatch-1 DO BEGIN
     periodic_distances,nomatch[ii].pos[0],nomatch[ii].pos[1],nomatch[ii].pos[2],hod.pos[0],hod.pos[1],hod.pos[2],250.0,distancesii
     wh_sort = sort(distancesii)
     nomatch[ii].iquenchcen = hod[wh_sort[1]].iquench
  ENDFOR

  hod[wh_nomatch] = nomatch

;  satellites.iquenchcen = -1
;  listmatch,satellites.hostID,hod.ID,A_ind,B_ind
;  satellites[A_ind].iquenchcen = hod[B_ind].iquench
;  wh_nomatch1 = where(satellites.iquenchcen eq -1)
;  satellites_unmatched1 = satellites[wh_nomatch1]
;  listmatch,satellites_unmatched1.subhostID,hod.ID,A_ind,B_ind
;  satellites_unmatched1[A_ind].iquenchcen = hod[B_ind].iquench
;  satellites[wh_nomatch1] = satellites_unmatched1
;  wh_nomatch2 = where(satellites.iquenchcen eq -1)
;  satellites_unmatched2 = satellites[wh_nomatch2]

return
end
;***********************************

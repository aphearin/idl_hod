;--------------------------------------------------------
function periodic_distance,pos1,pos2,Lbox
;pos1 is a 3-element array of the x,y,z position of a single object
;pos2 is a 3-element array of the x,y,z position of a single object
;Lbox is the size of the simulation
;output of function 'distance' is the 3-d distance between 
;--the two objects, accounting for box periodicity

  xdist = ABS(pos1[0]-pos2[0])
  ydist = ABS(pos1[1]-pos2[1])
  zdist = ABS(pos1[2]-pos2[2])

  IF (xdist gt Lbox/2.) THEN xdist = ABS(xdist-Lbox)
  IF (ydist gt Lbox/2.) THEN ydist = ABS(ydist-Lbox)
  IF (zdist gt Lbox/2.) THEN zdist = ABS(zdist-Lbox)

  distance = sqrt( xdist*xdist + ydist*ydist + zdist*zdist )

return,distance
end

;----------------------------------------
;--------------------------------------------------------
pro periodic_distances,x1,y1,z1,x2,y2,z2,Lbox,distances
;x1 is the input x-position of a single object
;y1 is the input y-position of a single object
;z1 is the input z-position of a single object
;x2 is a vector of input x-positions of N objects
;y2 is a vector of input y-positions of N objects
;z2 is a vector of input z-positions of N objects
;Lbox is the size of the simulation
;output is distances, a vector of dimension N 
;-- containing the 3d distance from the input object 
;-- with position (x1,y1,z1) to each of the N objects
;-- with positions given y (x2,y2,z2), 
;-- accounting for box periodicity

  xdist = ABS(x1-x2)
  ydist = ABS(y1-y2)
  zdist = ABS(z1-z2)

  whx=where(xdist gt Lbox/2.,ctx)
  IF (ctx gt 0) THEN xdist[whx] = ABS(xdist[whx]-Lbox)
;
  why=where(ydist gt Lbox/2.,cty)
  IF (cty gt 0) THEN ydist[why] = ABS(ydist[why]-Lbox)
;
  whz=where(zdist gt Lbox/2.,ctz)
  IF (ctz gt 0) THEN zdist[whz] = ABS(zdist[whz]-Lbox)

  distances = sqrt( xdist*xdist + ydist*ydist + zdist*zdist )

return
end

;----------------------------------------
pro periodic_projected_distances,x1,y1,x2,y2,Lbox,distances
;x1 is the input x-position of a single object
;y1 is the input y-position of a single object
;x2 is a vector of input x-positions of N objects
;y2 is a vector of input y-positions of N objects
;Lbox is the size of the simulation
;output is distances, a vector of dimension N 
;-- containing the 2d projected x-y distance from the input object 
;-- with position (x1,y1) to each of the N objects
;-- with positions given y (x2,y2), 
;-- accounting for box periodicity

  xdist = ABS(x1-x2)
  ydist = ABS(y1-y2)

  whx=where(xdist gt Lbox/2.,ctx)
  IF (ctx gt 0) THEN xdist[whx] = ABS(xdist[whx]-Lbox)
;
  why=where(ydist gt Lbox/2.,cty)
  IF (cty gt 0) THEN ydist[why] = ABS(ydist[why]-Lbox)
;
  distances = sqrt( xdist*xdist + ydist*ydist )

return
end
;----------------------------------------
;----------------------------------------
pro periodic_cylindrical_distances,x1,y1,z1,Vz1,x2,y2,z2,Vz2,Lbox,Hubble,projected_distances,relative_zvelocities
;x1 is the input x-position of a single object
;y1 is the input y-position of a single object
;z1 is the input z-position of a single object
;Vz1 is the input z peculiar velocity of a single object
;x2 is a vector of input x-positions of N objects
;y2 is a vector of input y-positions of N objects
;z2 is a vector of input z-positions of N objects
;Vz2 is a vector of input z peculiar velocities of N objects
;Lbox is the size of the simulation
;Hubble is the hubble rate in units of (km/s)/Mpc. 
;So if h=0.7, Hubble = 70.0
;output is projected_distances, a vector of dimension N 
;-- containing the 2d projected x-y distance from the input object 
;-- with position (x1,y1) to each of the N objects
;-- with positions given y (x2,y2), 
;-- accounting for box periodicity
;output is also relative_zvelocities, in km/s
;
;*** This routine gives screwy results, I think
;*** I'm not confident that I'm doing the redshift-space part correctly
;
  xdist = ABS(x1-x2)
  ydist = ABS(y1-y2)

  abszdist = ABS(z1-z2)
  dVz = Vz1 - Vz2
;
  whx=where(xdist gt Lbox/2.,ctx)
  IF (ctx gt 0) THEN xdist[whx] = ABS(xdist[whx]-Lbox)
;
  why=where(ydist gt Lbox/2.,cty)
  IF (cty gt 0) THEN ydist[why] = ABS(ydist[why]-Lbox)
;
  whz=where(abszdist gt Lbox/2.,ctz)
  IF (ctz gt 0) THEN abszdist[whz] = ABS(abszdist[whz]-Lbox)
;
  projected_distances = sqrt( xdist*xdist + ydist*ydist )

  relative_zvelocities = Hubble*abszdist + dVz

return
end
;----------------------------------------

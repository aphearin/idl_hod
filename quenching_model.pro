function active_fraction_allsats,frac_logM11,frac_logM15,logM
;
  slope = (frac_logM15-frac_logM11)/4.
  active_fraction_allsats = frac_logM11 + slope*(logM - 11.0)
  IF (logM lt 11) THEN active_fraction_allsats = frac_logM11
  IF (logM gt 15) THEN active_fraction_allsats = frac_logM15

return,active_fraction_allsats
end
;*************
function active_fraction_allcens,frac_logM11,frac_logM15,logM
;
  slope = (frac_logM15-frac_logM11)/4.
  active_fraction_allcens = frac_logM11 + slope*(logM - 11.0)
  IF (logM lt 11) THEN active_fraction_allcens = frac_logM11
  IF (logM gt 15) THEN active_fraction_allcens = frac_logM15

return,active_fraction_allcens
end
